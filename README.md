# Monster in Cuadrada

## Description

Game made for Ludum Dare 33.

## Compile

Install `node` and `npm` from the [Node's home page](https://nodejs.org/) and then the dependencies:

```sh
$ npm install
```

To compile simply do:

```sh
$ gulp build:debug
```

or:

```sh
$ gulp build:deploy
```

to generate an uglified version.


## Running

Theres a server supplied to run the game. Launch it with

```sh
$ node server/server.js
```

And browse to [localhost:5858](http://localhost:5858/)

