/// <reference path="../tsDefinitions/phaser.d.ts" />
/// <reference path="states/Boot.ts" />
/// <reference path="states/Preloader.ts" />
/// <reference path="states/Intro.ts" />
/// <reference path="states/Menu.ts" />
/// <reference path="states/Info.ts" />
/// <reference path="states/Main.ts" />
/// <reference path="states/GameOver.ts" />
/// <reference path="states/Win.ts" />

module MonsterInCuadrada {
	export class Game extends Phaser.Game {
		
		public static width: number = 400;
		public static height: number = 250;
		public static TILE_MONSTER_ID: number = 1025;
		public static TILE_SQUAREL_ID: number = 1026;
		public static TILE_SQUARENIE_ID: number = 1027;
		public static TILE_SQUATZUMA_ID: number = 1028;
		public static TILE_LTSQUARE_ID: number = 1029;
		
		constructor() {
			super(Game.width, Game.height, Phaser.AUTO, 'game', null);
			
			this.state.add('Boot', Boot);
			this.state.add('Preloader', Preloader);
			this.state.add('Intro', Intro);
			//this.state.add('MainMenu', MainMenu);
			this.state.add('Menu', Menu);
			//this.state.add('Game', Game);
			this.state.add('Main', Main);
			this.state.add('GameOver', GameOver);
			this.state.add('Info', Info);
			this.state.add('Win', Win);
		
			this.state.start('Boot');
		}
	}
}

// when the page has finished loading, create our game
window.onload = () => {
	var game = new MonsterInCuadrada.Game();
}
