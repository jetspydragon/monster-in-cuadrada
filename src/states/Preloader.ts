/// <reference path="../../tsDefinitions/phaser.d.ts" />

module MonsterInCuadrada {
	export class Preloader extends Phaser.State {
		preload() {
			this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
			this.load.path = 'assets/';
			
			// assumes files are key.png
			this.load.images(['intro1',
							 'intro2', 
							 'menu_bkg',
							 'info',
							 'monster',
							 'squarel',
							 'ltsquare',
							 'squarenie',
							 'squatzuma']);
			
			this.load.spritesheet('btn_start','btn_start.png', 158, 32);
			this.load.spritesheet('btn_info','btn_info.png', 158, 32);
			this.load.spritesheet('btn_back','btn_back.png', 158, 32);
			
			this.load.audio('monster_jump', 'Jump.ogg');
			this.load.audio('monster_hurt', 'hurt.ogg');	
		}
		
		create() {
			this.game.state.start('Intro');
		}
	}
}