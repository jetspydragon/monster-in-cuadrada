/// <reference path="../../tsDefinitions/phaser.d.ts" />
/// <reference path="../Game.ts" />
/// <reference path="Main.ts" />

module MonsterInCuadrada {
	export class Win extends Phaser.State {
		
		private btn_back: Phaser.Button;
		
		create() {
			this.game.add.image(0, 0, 'menu_bkg');
		
			this.btn_back = this.game.add.button(0, 0, 'btn_back', this.btnBackClick, this, 0, 2, 1);
			this.btn_back.anchor.setTo(0.5, 0.5);
			this.btn_back.position.setTo(Game.width / 2, 215);
			
			var winMessage = this.game.add.text(40, 120, Main.gameOverMessage, { font: "16px Arial Black", fill: "#FCD5A6" });
			winMessage.stroke = "#D19563";
			winMessage.strokeThickness = 2;
			winMessage.setShadow(2, 2, "#333333", 2, true, true);
			
			Main.currentMap = 0;
		}
		
		btnBackClick() {
			this.game.state.start('Menu');
		}
	}
}