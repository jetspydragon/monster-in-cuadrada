/// <reference path="../../tsDefinitions/phaser.d.ts" />
/// <reference path="../Game.ts" />
/// <reference path="../Sprites/Enemy.ts" />
module MonsterInCuadrada {
	export class Main extends Phaser.State {
		
		public static gameOverMessage: string = '';
		public static currentMap: number = 0;
		
		private monster: Phaser.Sprite;
		private layer1: Phaser.TilemapLayer;
		private map: Phaser.Tilemap[];
		private enemies: Phaser.Group;
		private monster_jump: Phaser.Sound;
		private monster_hurt: Phaser.Sound;
		private countText: Phaser.Text;
		
		private fps: number;
		private deltaTime: number;
		private canAdvance: boolean;
		private jumpTimer: number;
		private doubleJump: number;
		private cameraSpeed: number;
		private cameraDirection: number;
		
		private startDelay: number;
		private timePassed: number;
		
		preload() {		
			// Levels
			this.game.load.tilemap('level01', 'maps/level01.json', null, Phaser.Tilemap.TILED_JSON);
			this.game.load.tilemap('level02', 'maps/level02.json', null, Phaser.Tilemap.TILED_JSON);
			this.game.load.image('tiles', 'maps/tilemap.png');
            this.game.load.image('tiles2', 'maps/tilemap2.png');
		}
		
		create() {
			this.game.physics.startSystem(Phaser.Physics.ARCADE);
			this.game.physics.arcade.gravity.y = 350;
			this.game.input.mouse.capture = true;
			this.game.stage.backgroundColor = '#A6591E';
			//this.game.world.setBounds(0, 0, 200*16, 250);
				
			this.map = [];
			this.map[0] = this.game.add.tilemap('level01');
			this.map[0].addTilesetImage('tilemap', 'tiles');
            this.map[0].addTilesetImage('tilemap', 'tiles2');
			this.map[0].setCollisionByExclusion([Game.TILE_LTSQUARE_ID,
											Game.TILE_MONSTER_ID,
											Game.TILE_SQUAREL_ID,
											Game.TILE_SQUARENIE_ID,
											Game.TILE_SQUATZUMA_ID]);
			this.map[1] = this.game.add.tilemap('level02');
			this.map[1].addTilesetImage('tilemap', 'tiles');
			this.map[1].setCollisionByExclusion([Game.TILE_LTSQUARE_ID,
											Game.TILE_MONSTER_ID,
											Game.TILE_SQUAREL_ID,
											Game.TILE_SQUARENIE_ID,
											Game.TILE_SQUATZUMA_ID]);
	
			if (Main.currentMap >= this.map.length) {
				Main.gameOverMessage = 'You safe now monster... but some day...';
				this.game.state.start('Win');
			} else {
				this.layer1 = this.map[Main.currentMap].createLayer('Terrain');
				this.layer1.resizeWorld();
	
				this.enemies = this.game.add.group();
				this.enemies.classType = Enemy;
				this.enemies.enableBody = true;
				this.enemies.physicsBodyType = Phaser.Physics.ARCADE;
	
				this.monster = this.game.add.sprite(1, 1, 'monster');
				this.game.physics.arcade.enableBody(this.monster);
				this.monster.body.bounce.y = 0.1;
				this.monster.body.linearDamping = 1;
				this.monster.body.setSize(22, 26, 5, 5);
				//monster.body.setCircle(32);
				this.monster_jump = this.game.add.audio('monster_jump');
				this.monster_jump.volume = .1;
				this.monster_hurt = this.game.add.audio('monster_hurt');
				this.monster_hurt.volume = .1;
	
				//this.layer1.layer.data.forEach(this.parseLayer);
                this.layer1.getTiles(0, 0, this.layer1.layer.width, this.layer1.layer.height).forEach(this.parseLayer);
				//this.layer1.layer.data.forEach((element, index, array) => this.parseLayer(element, index, array));
				
				this.canAdvance = true;
				this.jumpTimer = 0;
				this.doubleJump = 0;
				this.cameraSpeed = 150;
				this.cameraDirection = 1;
				this.deltaTime = 0;
				this.fps = 0;
				this.game.camera.setPosition(0, 0);
				
				this.startDelay = 3;
				this.timePassed = 0;
				this.countText = this.game.add.text(this.camera.width/2-16, this.camera.height/2-32, '', { font: "32px Arial Black", fill: "#FCD5A6" });
			}
		}
		
		update() {
			this.deltaTime = this.game.time.elapsed / 1000;
			this.fps = Math.floor(1 / this.deltaTime);
			this.timePassed += this.deltaTime;
						
			this.game.physics.arcade.collide(this.monster, this.layer1, this.colMonsterLayer, null, this);
			this.game.physics.arcade.collide(this.monster, this.enemies, this.colMonsterEnemy, null, this);
			this.game.physics.arcade.collide(this.enemies, this.layer1, null, null, this);
			this.game.physics.arcade.collide(this.enemies, this.enemies, null, null, this);

			if (this.timePassed > this.startDelay) {
				this.countText.text = '';		
				this.game.camera.x += this.cameraDirection * this.cameraSpeed * this.deltaTime;
		
				this.moveMonster();
			} else {
				this.countText.text = (this.startDelay - Math.floor(this.timePassed)).toString();
			}
		}
		
		render() {
			//this.game.debug.font = '8px Courier';
			//this.game.debug.cameraInfo(this.game.camera, 32, 32);
			//this.game.debug.text('FPS:' + fps, 300, 32);
			//this.game.debug.body(monster);
			//this.game.debug.bodyInfo(monster, 32, 150);
			//this.game.debug.spriteCoords(this.baloon, 32, 300);
		}
		
		moveMonster() {
			if (this.canAdvance) {
				this.monster.body.velocity.x = this.cameraSpeed;
				//monster.body.moveRight(cameraSpeed);
			}
	
			if (this.monster.position.x >= (this.game.camera.x + Game.width/2) &&
					this.game.camera.x < this.game.world.width - this.game.camera.width) {
				this.monster.position.x = (this.game.camera.x + Game.width/2);
			}
	
			// Check input
			if (this.monster.body.onFloor()) {
				this.canAdvance = true;
				this.doubleJump = 0;
			}
			if (this.game.input.activePointer.isDown && this.game.time.now > this.jumpTimer &&
					((this.doubleJump === 0 && this.monster.body.onFloor()) || this.doubleJump === 2)) {
				this.monster.body.velocity.y = -200;
				this.monster_jump.play();
				//monster.body.moveUp(200);
				this.jumpTimer = this.game.time.now + 250;
				this.doubleJump++;
			}
			if (this.doubleJump === 1 && this.game.input.activePointer.isUp) {
				this.doubleJump++;
			}
	
			// Check monster still in the view
			if (!this.monster.inCamera) {
				if (this.monster.position.x >= this.game.world.width) {
					Main.currentMap++;
					this.game.state.start('Main');
				} else if (this.monster.position.x < this.game.camera.x ||
						this.monster.position.y > this.game.camera.height) {
					Main.gameOverMessage = 'Oops! You died horrible round creature!';
					this.game.state.start('GameOver');
				}
			}
		}
		
		colMonsterLayer(monster: Phaser.Sprite, layer: Phaser.Sprite) {
			if (!monster.body.onFloor() && monster.body.velocity.y > 0) {
				this.canAdvance = false;
			}
		}
		
		colMonsterEnemy(monster: Phaser.Sprite, enemy: Phaser.Sprite) {
			console.log('You been killed by ' + enemy.key);
			this.monster_hurt.play();
	
			Main.gameOverMessage = 'Yeah! ' + enemy.key + ' finally kill you!!!!';
			this.game.state.start('GameOver');
		}
		
		private parseLayer = (element: any, index: number, array: any) => {
            console.log("Index " + index);
			if (Object.prototype.toString.call(element) === '[object Array]') {
				element.forEach(this.parseLayer);
			} else {
				switch(element.index) {
					case Game.TILE_MONSTER_ID:
						this.monster.position.setTo(element.worldX, element.worldY);
						console.log('Monster create!');
						element.index = -1;
						break;
					case Game.TILE_SQUAREL_ID:
						this.enemies.create(element.worldX, element.worldY, 'squarel');
						console.log('Squarel create!');
						element.index = -1;
						break;
					case Game.TILE_LTSQUARE_ID:
						this.enemies.create(element.worldX, element.worldY, 'ltsquare');
						console.log('Lt. Square create!');
						element.index = -1;
						break;
					case Game.TILE_SQUARENIE_ID:
						this.enemies.create(element.worldX, element.worldY, 'squarenie');
						console.log('Squarenie create!');
						element.index = -1;
						break;
					case Game.TILE_SQUATZUMA_ID:
						this.enemies.create(element.worldX, element.worldY, 'squatzuma');
						console.log('Squatzuma create!');
						element.index = -1;
						break;
				}
			}
		}
	}
}