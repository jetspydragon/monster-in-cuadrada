/// <reference path="../../tsDefinitions/phaser.d.ts" />

module MonsterInCuadrada {
	export class Intro extends Phaser.State {
		
		private butIntro1: Phaser.Button;
		private butIntro2: Phaser.Button;
				
		create() {
			this.butIntro1 = this.game.add.button(0, 0, 'intro2', this.setIntro1, this);
			this.butIntro2 = this.game.add.button(0, 0, 'intro2', this.setIntro2, this);
		}
		
		setIntro1() {
			this.butIntro1.destroy();
		}
		
		setIntro2() {
			this.game.state.start('Menu');
		}
	}
}