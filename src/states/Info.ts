/// <reference path="../../tsDefinitions/phaser.d.ts" />
/// <reference path="../Game.ts" />

module MonsterInCuadrada {
	export class Info extends Phaser.State {
		
		private btn_back: Phaser.Button;
		
		create() {
			// Menu
			this.game.add.image(0, 0, 'info');
			
			this.btn_back = this.game.add.button(0, 0, 'btn_back', this.btnBackClick, this, 0, 2, 1);
			this.btn_back.anchor.setTo(0.5, 0.5);
			this.btn_back.position.setTo(Game.width / 2, 215);	
		}
		
		btnBackClick() {
			this.game.state.start('Menu');
		}
	}
}