/// <reference path="../../tsDefinitions/phaser.d.ts" />
/// <reference path="../Game.ts" />

module MonsterInCuadrada {
	export class Menu extends Phaser.State {
		
		private btn_start: Phaser.Button;
		private btn_info: Phaser.Button;
		
		create() {
			// Menu
			this.game.add.image(0, 0, 'menu_bkg');
			this.btn_start = this.game.add.button(0, 0, 'btn_start', this.btnStartClick, this, 0, 2, 1);
			this.btn_start.anchor.setTo(0.5, 0.5);
			this.btn_start.position.setTo(Game.width / 2, 125);	
			this.btn_info = this.game.add.button(0, 0, 'btn_info', this.btnInfoClick, this, 0, 2, 1);
			this.btn_info.anchor.setTo(0.5, 0.5);
			this.btn_info.position.setTo(Game.width / 2, 170);	
		}
		
		btnStartClick() {
			this.game.state.start('Main');
		}
		
		btnInfoClick() {
			this.game.state.start('Info');
		}
	}
}