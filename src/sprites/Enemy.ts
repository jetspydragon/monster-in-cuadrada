/// <reference path="../../tsDefinitions/phaser.d.ts" />

module MonsterInCuadrada {
	export class Enemy extends Phaser.Sprite {
		constructor(game: Phaser.Game, x: number, y: number, key: string, frame: string) {
			super(game, x, y, key, frame);
		}
		
		update() {
			var velocityX = 0;
			switch(this.key) {
				case 'squarel':
					velocityX = 50;
					break;
				case 'ltsquare':
					velocityX = 100;
					break;
				case 'squarenie':
					velocityX = 100;
					break;
				case 'squatzuma':
					velocityX = 200;
					break;
			}
			if (this.inCamera) {
				//this.body.moveLeft(velocityX);
				this.body.velocity.x = -velocityX;
			}
		}
	}
}